<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Constants\UserRole;
use App\Constants\ObjectStatus;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->id = \Str::uuid();
        $user->name  = 'Admin';
        $user->email = 'admin@site.com';
        $user->password = bcrypt('password');
        $user->role = UserRole::ADMIN;
        $user->status = ObjectStatus::ACTIVE;

        $user->save();
    }
}
