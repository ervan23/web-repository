<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => '/'], function () {
    Route::get('/', function() {
        return redirect()->route('cms.auth.login');
    });
});

Route::group(['prefix' => '/admin', 'namespace' => 'Admin', 'middleware' => ['auth.web']], function () {
    Route::get('/', 'DashboardController@index')->name('cms.dashboard.index');
});

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    Route::get('/login', 'AuthController@login')->name('cms.auth.login');
    Route::post('/signin', 'AuthController@signin')->name('cms.auth.signin');
    Route::get('/signout', 'AuthController@signout')->middleware('auth.web')->name('cms.auth.signout');
});
