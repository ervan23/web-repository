<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use App\User;
use App\Repositories\UserRepository;

class AuthController extends Controller
{

    private $repository;

    public function __construct(UserRepository $repository) {
        $this->repository = $repository;
    }

    public function login() {
        if(\Auth::check()) {
            if(\Auth::user()->isActive())
                return redirect()->route('cms.dashboard.index');

            return $this->signout();
        }

        return view('cms.auth.login');
    }

    public function signin(Request $req) {
        $validate = Validator::make($req->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validate->fails()) {
            return back()->withInput()->withErrors($validate->errors());
        }

        $credential = $req->only('email', 'password');
        $user = $this->repository->signin($credential);

        if(!$user) {
            return back()->withInput()->withErrors(['error' => ['Email or Password is Invalid']]);
        }

        if(!$user->isActive()) {
            Auth::logout();
            return back()->withInput()->withErrors(["error" => ['Please activate your account first']]);
        }

        return redirect()->route('cms.dashboard.index');
    }

    public function signout() {
        Auth::logout();

        return redirect()->route('cms.auth.login');
    }
}
