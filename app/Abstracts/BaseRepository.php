<?php

namespace App\Abstracts;

use App\Interfaces\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;


abstract class BaseRepository implements RepositoryInterface {

    protected $model;
    protected $request;

    public function __construct(Model $model, Request $request = null) {
        $this->model = $model;
        $this->request = $request;
    }

}