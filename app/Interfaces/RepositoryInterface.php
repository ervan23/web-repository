<?php

namespace App\Interfaces;

interface RepositoryInterface {

    public function getPaginate($page, $show);
    public function getAll($limit);
    public function getById($id);
    public function getBy($column, $value);
    public function update($data);
    public function insert($data);
    public function deleteBy($column, $id);
}