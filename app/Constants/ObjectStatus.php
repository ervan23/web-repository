<?php

namespace App\Constants;

final class ObjectStatus {

    const INACTIVE       = 0;
    const ACTIVE         = 1;

    const LIST = [
        'inactive',
        'active',
    ];

    public static function getString($status) {
        return self::LIST[$status];
    }

    public static function getHtml($status) {
        switch ($status) {
			case 1:
				return '<span class="badge badge-success">Active</span>';
    		case 0:
    			return '<span class="badge badge-warning">In Active</span>';
    	}
    }

}
