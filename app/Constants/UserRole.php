<?php

namespace App\Constants;

final class UserRole {

    const BASIC_USER    = 0;
    const ADMIN         = 1;
    const DOSEN         = 2;
    const MAHASISWA     = 3;

    const LIST = [
        'pengguna',
        'admin',
        'dosen',
        'mahasiswa'
    ];

    public static function getString($status) {
        return self::LIST[$status];
    }

    public static function getHtml($status) {
        switch ($status) {
			case 0:
				return '<span class="badge badge-info">Pengguna</span>';
    		case 1:
    			return '<span class="badge badge-primary">Admin</span>';
    		case 2:
    			return '<span class="badge badge-success">Dosen</span>';
    		case 3:
    			return '<span class="badge badge-warning">Mahasiswa</span>';
    	}
    }

}
