<?php

namespace App\Repositories;

use App\Abstracts\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class UserRepository extends BaseRepository {

    public function __construct(User $user, Request $request) {
        parent::__construct($user, $request);
    }

    public function getPaginate($page, $show) {}
    public function getAll($limit = 10) {}
    public function getById($id) {}
    public function getBy($column, $value) {}
    public function update($data) {}
    public function insert($data) {}
    public function deleteBy($column, $value) {}

    public function signin(array $credentials) {
        if(!Auth::attempt($credentials)) {
            return null;
        }

        return Auth::user();
    }
}