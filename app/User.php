<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Constants\UserRole;
use App\Constants\ObjectStatus;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'role', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin() {
        return $this->role == UserRole::ADMIN;
    }

    public function isUser() {
        return $this->role == UserRole::BASIC_USER;
    }

    public function isDosen() {
        return $this->role == UserRole::DOSEN;
    }

    public function isMahasiwa() {
        return $this->role == UserRole::MAHASISWA;
    }

    public function getBadge() {
        return UserRole::getHtml($this->role);
    }

    public function getLabel() {
        return UserRole::getString($this->role);
    }

    public function isActive() {
        return $this->status == ObjectStatus::ACTIVE;
    }

    public function scopeActive($query) {
        return $query->where('status', ObjectStatus::ACTIVE);
    }
}
