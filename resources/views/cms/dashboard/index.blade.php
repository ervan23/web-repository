<!DOCTYPE html>
<html lang="en-EN">
    <head>
        @include('cms.includes.head')
    </head>
<body>
    <div class="app text-center" id="app" style="padding-top: 50px;">
        <h1>Hi, You're loggedin</h1>
        <a href="{{route('cms.auth.signout')}}" class="btn btn-danger">Sign out</a>
    </div>
    @include('cms.includes.foot')
</body>
</html>

