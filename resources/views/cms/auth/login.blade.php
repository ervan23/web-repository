<!DOCTYPE html>
<html lang="en-EN">
    <head>
        @include('cms.includes.head')
    </head>
<body>
    <div class="app" id="app">

        <!-- ############ LAYOUT START-->
        <div class="center-block w-xxl w-auto-xs p-y-md">
            <div class="navbar">
                <div class="pull-center">
                    <div>
                        <a class="navbar-brand"><img src="{{ URL::to('backEnd/assets/images/logo.png') }}" alt=".">
                        <span class="hidden-folded inline">Web Repository</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="p-a-md box-color r box-shadow-z1 text-color">
                <div class="m-b text-sm">
                    Sign in to your account
                </div>
                <form name="form" method="POST" action="{{ route('cms.auth.signin') }}">
                    {{ csrf_field() }}
                    @if($errors ->any())
                        <div class="alert alert-danger m-b-0">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            @foreach($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif

                    <div class="md-form-group float-label {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" name="email" value="{{ old('email') }}" class="md-input" required>
                        <label>Email</label>
                    </div>
                    <div class="md-form-group float-label {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" name="password" class="md-input" required>
                        <label>Password</label>
                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    <div class="m-b-md">
                        <label class="md-check">
                            <input type="checkbox" name="remember"><i
                                class="primary"></i> Remember me
                        </label>
                    </div>
                    <button type="submit" class="btn primary btn-block p-x-md m-b">Sign In</button>
                </form>
                <hr/>

                <div class="p-v-lg text-center">
                    <div class="m-t">
                        <a href="#" class="text-primary _600">Forgot password ?</a>
                    </div>
                </div>

            </div>



        </div>

        <!-- ############ LAYOUT END-->


    </div>
    @include('cms.includes.foot')
</body>
</html>

